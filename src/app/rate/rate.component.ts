import { Component, OnInit } from '@angular/core';

import { Rate } from '../rate';
import { RateService } from './rate.service';

@Component({
  selector: 'app-rate',
  templateUrl: './rate.component.html',
  providers: [RateService],
  styleUrls: ['./rate.component.css']
})

export class RateComponent implements OnInit {

  rates: Rate[] = [];
  rateBaseCode = 'MZN';
  constructor(private rateService: RateService) {}

  ngOnInit() {
    this.getRates(this.rateBaseCode);
  }

  getRates(rateBaseCode:string): Rate[]{

    this.rateService.getRates(rateBaseCode)
    // .subscribe(rates => this.rates = rates)
    .subscribe((response: any)=>{
        const { rates } = response;
        this.rates.length=0;
        Object.keys(rates).forEach((key: string) => {
          this.rates.push({
            code: key,
            value: rates[key],
          })
        })
      });
      // console.log(this.rates.length);
      return this.rates;
  }

}
