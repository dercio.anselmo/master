import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Rate } from '../rate';
import { Observable } from 'rxjs';

@Injectable()
export class RateService {
    // rateUrl = 'https://open.er-api.com/v6/latest/';
    rateUrl = 'https://exchange-rate-teste-api.herokuapp.com/';
    // rateUrl = 'http://localhost:8080/';

    constructor(private http: HttpClient) { }

     /** GET rates from the server */
    getRates(rateBaseCode: string): Observable<Rate[]> {
        return this.http.get<Rate[]>(this.rateUrl+rateBaseCode);
    }

}
